const {Schema, model, Types} = require('mongoose');

const SubCategorySchema = new Schema({
    category_id: {  
        type: Types.ObjectId,
        ref: "categories",
        required: true

    },
    sub_category_name: {
        type: String,
        required: true
    }
});


module.exports = model('Sub-Category', SubCategorySchema);
