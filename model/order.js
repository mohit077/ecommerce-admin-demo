const {Schema, model, Types} = require('mongoose');

const OrderSchema = new Schema(
  {
    user_id: {
        type: Types.ObjectId,
        ref: 'Register',
        required: true,
    },
    product_id:{
        type: Types.ObjectId, 
        ref:'products'
    },
    qty:{
        type:Number, 
        required:true
    },
    price: {
        type: Number,
        required: true
    },
    // tax: {
    //     type: Number,
    //     default: 99
    // },
    // shippingFee: {
    //     type: Number,
    //     default: 40
    // // },
    // total: {
    //     type: Number,
    //     required: true,
    //  },
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    mobile: {
        type: Number,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    pincode: {
        type: Number,
        required: true
    },
  },
  { timestamps: true }
);

module.exports = model('Order', OrderSchema);
