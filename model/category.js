const { Schema, model } = require('mongoose');


const CategorySchema = new Schema({
    name: {
        type: String,
        required: [true, "Please Add Category Name"],
        minlength: 2,
        maxlength: 50,
      },
});

module.exports = model('Category', CategorySchema);