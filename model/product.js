const {Schema, model, Types} = require('mongoose');
const multer = require('multer');

const ProductSchema = new Schema({
    product_name: {
        type: String,
        required: true,
    },
    category_id: [{
        type: Types.ObjectId,
        ref: "Category"
    }],
    subcategory_id: {
        type: Types.ObjectId,
        ref: "sub-categories",
        required: true
    },
    product_details: {
        type: String,
        required: true,
    },
    product_image: [
        { img: { type: String } }
    ],
    qty:{
        type:Number, 
        required:true
    },
    price:{
        type:Number, 
        required:true
    }
});





module.exports = model('products', ProductSchema);