const Category = require('../model/category');
const SubCategory = require('../model/subCategory');
const Joi = require('joi');

const AddCategory = async (req, res) => {

    try {
        // Get Category Input
        const {name}  = req.body;

        const schema = Joi.object().keys({
            name: Joi.string().min(3).max(25).trim(true).required(),
        })
    
        const result = schema.validate(req.body);
        const { value, error } = result
        const valid = error == null
        if (!valid) {
            return res.status(200).send({ message: error.message, success: false, data: {} })
        }
        else {
            // Check if Category Already Exists
            const categorylist = await Category.findOne({ name });

            if(categorylist) {
                return res.status(409).json({ message: 'Category Already Exists Please Add Something Different', success: false, data: {} });
            }

            const AddCategory = await Category.create({
                name
            });

            return res.status(200).json({ message: `${AddCategory.name} = Category Add Successfully`, success: true, data: { AddCategory } });
        }

    } catch (error) {
        return res.status(200).send({ message: error.message, success: false, data: {} });
    }

  

}
 

const AddSubCategory = async (req, res) => {

    try {
        // Get Category Input
        const {category_id, sub_category_name}  = req.body;

        const schema = Joi.object().keys({
            category_id: Joi.string().trim(true).required(),
            sub_category_name: Joi.string().min(3).max(25).trim(true).required(),
        })
    
        const result = schema.validate(req.body);
        const { value, error } = result
        const valid = error == null
        if (!valid) {
            return res.status(200).send({ message: error.message, success: false, data: {} })
        }
        else {
            // Check Parent Category 
            const category = await Category.findById({_id:category_id});
            if(!category){
                return res.status(400).json({ message: "Please Use Valid Category Id", success: false, data: {} });
            }

            // Check Duplicate SubCategory
            const subcategorydata = await SubCategory.findOne({ sub_category_name });
            if(subcategorydata){
                return res.status(400).json({ message: "This SubCategory Already Exits", success: false, data: {} });
            }

            const SubCategoryData = await SubCategory.create({
                category_id,
                sub_category_name
            });

            return res.status(200).json({ message: `Subcategory Add Successfully`, success: true, data: {SubCategoryData} });
        }

    } catch (error) {
        return res.status(200).send({ message: error.message, success: false, data: {} });
    }
    

    

}


module.exports = {
    AddCategory,
    AddSubCategory
}