const Category = require('../model/category');
const SubCategory = require('../model/subCategory');
const Product = require('../model/product');
const Joi = require('joi');

const { Types } = require('mongoose');

const AddProduct = async (req, res) => {

    try {
      // Get Product Input
      const {product_name, category_id, subcategory_id, product_details, qty, price} = req.body;

      const schema = Joi.object().keys({
        product_name: Joi.string().min(2).max(25).required(),
        category_id: Joi.string().min(3).max(25).required(),
        subcategory_id: Joi.required(),
        product_details: Joi.string().min(10).required(),
        qty: Joi.string().required(),
        price: Joi.string().required(),
    })

    const result = schema.validate(req.body);
    const { value, error } = result
    const valid = error == null
    if (!valid) {
        return res.status(200).send({ message: error.message, success: false, data: {} })
    }
    else {
          let product_image = [];

          if (req.files.length > 0) {
              product_image = req.files.map((file) => {
              return { img: file.location };
            });
          }
      
          // Check if user already exist
          const isAlreadyAdd = await Product.findOne({ product_name });
      
          if(isAlreadyAdd) {
              return res.status(409).json({ message: 'Product Name Already Exist', success: false, data: {} });
          }
      
          // Check Valid Category_ID
          const category = await Category.findById({_id: category_id});
          if(!category){
              return res.status(400).json({ message: "Please Use Valid Category Id", success: false, data: {} });
          }
      
          // Check Valid SubCategory_ID
          const subcategory = await SubCategory.findById({_id: subcategory_id});
          if(!subcategory){
              return res.status(400).json({ message: "Please Use Valid SubCategory Id", success: false, data: {} });
          }
      
          
          const ProductData = await Product.create({
              product_name,
              category_id,
              subcategory_id,
              product_details,
              product_image,
              qty,
              price
          });
      
          return res.status(200).json({ message: `Product Add Successfully`, success: true, data: {ProductData} });
  
    }
      
    } catch (error) {
      return res.status(400).send({ message: error.message, success: false, data: {} });
    }

    
    
}


const AllProduct = async (req, res) => {
    try {

        // const product = await Product.find({}).populate("category_id").exec();

        const product = await Product.aggregate([
            {
              '$lookup': {
                'from': 'categories', 
                'localField': 'category_id', 
                'foreignField': '_id', 
                'as': 'category_id'
              }
            }, {
              '$unwind': {
                'path': '$category_id', 
                'preserveNullAndEmptyArrays': true
              }
            }, {
              '$lookup': {
                'from': 'sub-categories', 
                'localField': 'subcategory_id', 
                'foreignField': '_id', 
                'as': 'subcategory_id'
              }
            }, {
              '$unwind': {
                'path': '$subcategory_id', 
                'preserveNullAndEmptyArrays': true
              }
            }, {
              '$unset': [
                'subcategory_id.category_id'
              ]
            }
        ]);

	    return res.json({ err: 0, message: 'Success', data: product });

    }
    catch (error) {
        return res.status(400).json({ message: error.message, success: false, data: {} });
    }   
}

const GetProduct = async (req, res) => {
    try {
        const id = (req.params.id).trim();

      // const product = await Product.findById(id).populate("category_id").exec();

      // Check Valid Product
        const product = await Product.aggregate([
          {
            '$match': {
              '_id': Types.ObjectId(id)
            }
          }, {
            '$lookup': {
              'from': 'categories', 
              'localField': 'category_id', 
              'foreignField': '_id', 
              'as': 'category_id'
            }
          }, {
            '$unwind': {
              'path': '$category_id', 
              'preserveNullAndEmptyArrays': true
            }
          }, {
            '$lookup': {
              'from': 'sub-categories', 
              'localField': 'subcategory_id', 
              'foreignField': '_id', 
              'as': 'subcategory_id'
            }
          }, {
            '$unwind': {
              'path': '$subcategory_id', 
              'preserveNullAndEmptyArrays': true
            }
          }
        ]);

      if(!product){
          return res.status(400).json({ message: "Please Use Valid Product Id. Not Product Match", success: false, data: {} });
      }

      return res.json({ err: 0, message: 'Success', data: product });

    }
    catch (error) {
        return res.status(400).json({ message: error.message, success: false, data: {} });
    }   
}

const GetProductByCategoryID = async (req, res) => {
    try {
        const id = req.params.id;

        // const ProductByCategory = await Product.find({ category_id: id }).populate("category_id").exec();


        const ProductByCategory = await Product.aggregate([
          {
            '$match': {
              'category_id': Types.ObjectId(id)
            }
          }, {
            '$lookup': {
              'from': 'categories', 
              'localField': 'category_id', 
              'foreignField': '_id', 
              'as': 'category_id'
            }
          }, {
            '$unwind': {
              'path': '$category_id', 
              'preserveNullAndEmptyArrays': true
            }
          }, {
            '$lookup': {
              'from': 'sub-categories', 
              'localField': 'subcategory_id', 
              'foreignField': '_id', 
              'as': 'subcategory_id'
            }
          }, {
            '$unwind': {
              'path': '$subcategory_id', 
              'preserveNullAndEmptyArrays': true
            }
          }
        ]);
        if(ProductByCategory.length == 0){
            return res.status(400).json({ message: "Please Use Valid Category Id. Not Match Any Products", success: false, data: {} });
        }

        return res.json({ err: 0, message: 'Success', data: ProductByCategory });

    }
    catch (error) {
        return res.status(400).json({ message: error.message, success: false, data: {} });
    }   

}

module.exports = {
    AddProduct,
    AllProduct,
    GetProduct,
    GetProductByCategoryID
}