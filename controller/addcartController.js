const Cart = require('../model/addcart');
const Product = require('../model/product');
const Joi = require('joi');

const AddToCart = async (req, res) => {
    try {
        const { product_id, qty } = req.body;

        const schema = Joi.object().keys({
            product_id: Joi.string().required(),
            qty: Joi.string().required()
        });

        const result = schema.validate(req.body);
        const { value, error } = result
        const valid = error == null
        if (!valid) {
            return res.status(400).send({ message: error.message, success: false, data: {} })
        }
        else{
            const user = req.user

            const productDetails = await Product.findOne({ _id: product_id });
            if (productDetails.qty >= qty) {
                const cartData = await Cart.findOne({ user_id: user.id });
                var productData
                    if (!cartData) {
                        await Cart.create({
                            user_id: user.id,
                            products: [{ product_id, price: productDetails.price * qty, qty: qty }],
                            price: productDetails.price * qty
                        });

                        productData = await Cart.findOne({ user_id: user.id }).populate({ path: 'products', populate: { path: 'product_id', select: '_id product_name' } })
                    }
                    else {
                        let cartProduct = await cartData.products
                        let findProduct = cartProduct.find(c => c.product_id == product_id)
                        if (findProduct) {
                            productData = await Cart.findOneAndUpdate({
                                user_id: user.id,
                                'products.product_id': product_id
                            }, {
                                $set: { 'products.$.qty': qty, 'price': (cartData.price - findProduct.price) + productDetails.price * qty, 'products.$.price': productDetails.price * qty }

                            },
                                { new: true }).populate({ path: 'products', populate: { path: 'product_id', select: '_id product_name' } })
                        }
                        else{
                            productData = await Cart.findOneAndUpdate({
                                user_id:user.id,
                            },{
                                $push:{products:{product_id, price:productDetails.price*qty, qty:qty}}, $set:{'price':cartData.price+productDetails.price*qty}
                            },{
                                new:true
                            }).populate({ path: 'products', populate: { path: 'product_id', select: '_id product_name' } })
                        }
                    }
                    return res.status(200).send({ message: 'Added to Cart', success: true, data: productData })

            }else{
                return res.status(200).send({ message: 'Please Select Valid Qty ( Out Of Stock ).', success: false, data: {} })
            }
        }
    } catch (error) {
        console.log(error);
        return res.status(400).send({ message: error.message, success: false, data: {} });
    }
}

const CartProduct = async (req, res) => {
    try {
        const user = req.user

        const cartData = await Cart.aggregate([
            {
                '$lookup': {
                    'from': 'registers',
                    'localField': 'user_id',
                    'foreignField': '_id',
                    'as': 'user_details'
                }
            }, {
                '$unwind': {
                    'path': '$user_details',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$unwind': {
                    'path': '$products',
                    'preserveNullAndEmptyArrays': true
                }
            }, {
                '$lookup': {
                    'from': 'products',
                    'localField': 'products.product_id',
                    'foreignField': '_id',
                    'as': 'products.product_id'
                }
            }, {
                '$unwind': {
                    'path': '$products.product_id',
                    'preserveNullAndEmptyArrays': true
                }
            },
            {
                '$lookup': {
                    'from': 'registers',
                    'localField': 'registers._id',
                    'foreignField': 'user_details',
                    'as': 'user_reg_details'
                }
            }, {
                '$unwind': {
                    'path': '$user_reg_details',
                    'preserveNullAndEmptyArrays': true
                }
            },

             {
                '$unset': [
                    'user_id.password'
                ]
            }, {
                '$group': {
                    '_id': '$_id',
                    'products': {
                        '$push': '$products'
                    },                  
                    'user_id': {
                        '$first': '$user_reg_details'
                    },
                    'price': {
                        '$first': '$price'
                    }
                }
            }
        ])
        return res.status(200).send({ message:'Success', success: true, data: cartData })
     } catch (error) {
         return res.status(200).send({ message: error.message, success: false, data: {} })
     }
}

const RemoveCartProduct = async (req, res) => {
    try {
        const user = req.user;

        const { product_id } = req.body;
        const productDetails = await Cart.findOne({user_id:user.id, 'products.product_id':product_id});

        const products = productDetails.products

        const findProduct = products.find(p=>p.product_id==product_id)
        if(findProduct){

         const cartDetails = await Cart.findOneAndUpdate({user_id:user.id, 'products.product_id':product_id},{$pull:{products:{product_id:product_id}}, $set:{price:productDetails.price-findProduct.price}},{new:true})
         .populate({ path: 'products', populate: { path: 'product_id', select: '_id product_name brand qty' } })
         return res.status(200).send({ message:'Success', success: true, data: cartDetails })

        }
        
     } catch (error) {
         console.log(error);
         return res.status(200).send({ message: error.message, success: false, data: {} })
     }
}

module.exports = {
    AddToCart,
    CartProduct,
    RemoveCartProduct
}