const Register = require('../model/register');
const jwt = require('jsonwebtoken');
const Joi = require('joi');

const registerUser = async (req, res) => {

    // Get User Input
    const {firstname, lastname, email, password} = req.body;

    const schema = Joi.object().keys({
        firstname: Joi.string().alphanum().min(3).max(25).trim(true).required(),
        lastname: Joi.string().alphanum().min(3).max(25).trim(true).required(),
        email: Joi.string().email().trim(true).required(),
        password: Joi.string().min(8).trim(true).required(),
    })

    const result = schema.validate(req.body);
    const { value, error } = result
    const valid = error == null
    if (!valid) {
        return res.status(200).send({ message: error.message, success: false, data: {} })
    }

    // Check if user already exist
    const isAlreadyExist = await Register.findOne({ email });

    if(isAlreadyExist) {
        if(isAlreadyExist.status === 0) return res.status(409).json({ message: 'Account Alreadt Exist Please Wait For Approval', success: false, data: {} });
        return res.status(409).json({ message: 'Account Already Exist. Please Login', success: false, data: {} });
    }

    const isFirstAccount = (await Register.countDocuments({})) === 0;

    const role = isFirstAccount ? "admin" : "user";

    const user = await Register.create({
        firstname,
        lastname,
        email,
        password,
        role
    });

    // Password Remove to Response
    const payload = {
        firstname,
        lastname,
        email,
        role    
    }

    return res.status(200).json({ message: `Register Complete ${firstname} ${lastname} = ${role}`, success: true, data: {payload} });
}


const login = async (req, res) => {

    try {
        
        const { email, password } = req.body; 

        const schema = Joi.object().keys({
            email: Joi.string().email().trim(true).required(),
            password: Joi.string().trim(true).required(),
        })
    
        const result = schema.validate(req.body);
        const { value, error } = result
        const valid = error == null
        if (!valid) {
            return res.status(200).send({ message: error.message, success: false, data: {} })
        }
        else {

            if (!(email && password)) {
                return res.send({ err: 1, message: "Please provide email and password", success: false, data: {} });
            }
        
            const user = await Register.findOne({ email });
            if (user) {
                if (user.status === 0) return res.status(409).json({ message: "Please Wait For Approval", success: false, data: {} });
            
            }
            else{
                return res.status(400).json({ message: 'Invails Credentials', success: false, data: {} });
            }
        
            const isPasswordCorrect = await user.comparePassword(password);
            if (!isPasswordCorrect) {
                return res.status(400).json({ message: `Password invaild`, success: true, data: {} });
              
            }
          
            const token = jwt.sign({id: user._id}, process.env.TOKEN_SECRET);
            // res.header('auth-token', token).send(token);
        
            // Password Remove to Response
            const MyRes = await Register.findById({ _id: user.id},{password:0});
        
            // Login
            return res.json({ err: 0, message: "Login Success", success: true, data: {MyRes,token} });
        }

    } catch (error) {
        return res.status(400).send({ message: error.message, success: false, data: {} })
    }

    

    
    
}



module.exports = {
    registerUser,
    login
};