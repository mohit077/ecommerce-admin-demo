
const Order = require('../model/order');
const Cart = require('../model/addcart');
const Product = require('../model/product');
const Joi = require('joi');

const OrderPlace = async (req, res) => {
    try {
        // Get Input
        const { name, email, mobile, address, pincode } = req.body;

        const schema = Joi.object().keys({
            name: Joi.string().alphanum().min(3).max(25).trim(true).required(),
            email: Joi.string().email().trim(true).required(),
            mobile: Joi.string().length(10).pattern(/[6-9]{1}[0-9]{9}/).required(),
            address: Joi.string().required(),
            pincode: Joi.string().min(6).max(6).required()
        })

        const result = schema.validate(req.body);
        const { value, error } = result
        const valid = error == null
        if (!valid) {
            return res.status(400).send({ message: error.message, success: false, data: {} })
        }
        else{
            const user = req.user;
            const CartRecord = await Cart.findOne({ user_id: user.id });
            if (CartRecord && CartRecord.products.length) {
                const products = CartRecord.products

                for (let i = 0; i < products.length; i++) {
                    const ProductCheck = await Product.findOne({_id: products[i].product_id, qty: { $gte: products[i].qty} });
                    if(!ProductCheck) return res.status(200).send({ message: 'Please Stock Out Product Remove', success: false, data: {} })
                }

                for (let i = 0; i < products.length; i++) {
                    await Order.create({
                        user_id: user.id,
                        product_id:  products[i].product_id,
                        qty: products[i].qty,
                        price: products[i].price,
                        name,
                        email,  
                        mobile,
                        address,
                        pincode,
                    });
                    await Product.findOneAndUpdate({ _id: products[i].product_id }, { $inc: { qty: -products[i].qty } });
                    await Cart.findOneAndUpdate({ user_id: user.id }, { $set: { products: [], price: 0 } });
                }
                return res.status(200).send({ message: 'Your Order Place Successfully', success: true, data: {} })

            }else{
                return res.status(200).send({ message: 'please Product Add Your Cart', success: false, data: {} })
            }
        } 

    } catch (error) {
        return res.status(200).send({ message: error.message, success: false, data: {} })
    }   
}


module.exports = {
    OrderPlace
}