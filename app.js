require('dotenv').config();
require('./config/mongoose').connect();
const express = require('express');
const port = 3001;

const app = express();
const cors = require('cors');

const registerRouter = require('./routes/register');
const categoryRouter = require('./routes/category');
const productRouter = require('./routes/product');
const cartRouter = require('./routes/cart');
const orderRouter = require('./routes/order');

app.use(cors({
    origin:'*'
}));

app.use(express.json());
app.use('/admin', registerRouter);
app.use('/category', categoryRouter);
app.use('/product', productRouter);
app.use('/cart', cartRouter);
app.use('/order', orderRouter);

app.listen(port, function(err){
    if(err){
        console.log(err);
    }
    console.log(`server is running on port ${port}`)
});

module.exports = app;