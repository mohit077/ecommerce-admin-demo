const mongoose = require('mongoose');

const { MONGO_URI } = process.env;

exports.connect = () => {
    mongoose.connect(MONGO_URI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    }).then(() => {
        console.log("DB Connect");
    }).catch((error) => {
        console.log("Connection field");
        console.log(error);
        process.exit(1);
    });
};