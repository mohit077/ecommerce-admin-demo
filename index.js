const app = require('./app');

app.listen(port, function(err){
    if(err){
        console.log(err);
    }
    console.log(`server is running on port ${port}`)
});
