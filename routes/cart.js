const express = require('express');
const routes = express.Router();

const AddCartController = require('../controller/addcartController');
const verify = require('../middleware/verifyToken');

routes.post('/addcart',verify, AddCartController.AddToCart);

routes.get('/cartproduct',verify, AddCartController.CartProduct);

routes.post('/removecartproduct', verify, AddCartController.RemoveCartProduct);

module.exports = routes;