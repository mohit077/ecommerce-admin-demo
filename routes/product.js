const express = require('express');


const { uploadS3 } = require('../middleware/fileupload');
const multer = require("multer");
const routes = express.Router();

const multerS3 = require('multer-s3');
const shortid = require("shortid");
const path = require("path");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, path.join(path.dirname(__dirname), "uploads"));
    },
    filename: function (req, file, cb) {
      cb(null, shortid.generate() + "-" + file.originalname);
    },
  });

  const upload = multer({ storage });

const ProductController = require('../controller/productController');
const verify = require('../middleware/verifyToken');

routes.post('/addproduct', uploadS3.array("product_image"), ProductController.AddProduct);

// Get All Product
routes.get('/allproduct',verify, ProductController.AllProduct);

// Get Product By Id
routes.get('/getproduct/:id',verify, ProductController.GetProduct);

// Get product by category Id
routes.get('/getproductbycategory/:id',verify, ProductController.GetProductByCategoryID);

module.exports = routes;