const express = require('express');
const routes = express.Router();

const CategoryController = require('../controller/categoryController');
const verify = require('../middleware/verifyToken');

routes.post('/addcategory',verify, CategoryController.AddCategory);

routes.post('/subcategory',verify, CategoryController.AddSubCategory);

module.exports = routes;