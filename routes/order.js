const express = require('express');
const routes = express.Router();

const OrderController = require('../controller/orderController');
const verify = require('../middleware/verifyToken');

routes.post('/orderplace', verify, OrderController.OrderPlace);

module.exports = routes;